import os
from configparser import ConfigParser

import numpy as np
import pandas as pd
import seaborn as sns
from pymongo import MongoClient
from matplotlib import pyplot as plt

def query_pipeline(columns, collection, sample_size, source):
    file_name = f'data/{source}_{sample_size}.csv.gz'
    try:
        os.mkdir('data')
    except Exception:
        pass
    if os.path.exists(file_name):
        df = pd.read_csv(file_name)
    else:
        mongo_filter = {col: {"$exists":True} for col in columns}
        mongo_project = {f'{col}':f'${col}' for col in columns}
        mongo_agg = [{"$match": mongo_filter},
                     {"$project": mongo_project},
                     {'$sample': {'size': sample_size}}]
        query_results = collection.aggregate(mongo_agg, allowDiskUse=True)
        df = pd.DataFrame.from_records(query_results)
        df =df.drop(labels=['_id'], axis=1)
        df['source'] = source
        df.to_csv(file_name)
    return df

HOME_FOLDER = "/Users/mbakos"

# read DB credentials
parser = ConfigParser()
parser.read(os.path.join(HOME_FOLDER, ".ssl/research_db.auth"))
config = parser["research-db.avawomen.com"]

# connect to mongoDB
client = MongoClient("research-db.avawomen.com", ssl=True,
                     ssl_certfile=os.path.join(HOME_FOLDER, ".ssl/research-db-user1-combined.pem"))
client.admin.authenticate(name=config["username"], password=config["password"])


ttc_cols = ['temperature_skin_99', 'br_50', 'hr_10', 'hrv_rmssd_50', 'total_sleep']
collection_ttc = client.rnd.health_trainvalsplit
cc_cols = ['temp_skin_percentile_99', 'br_percentile_50', 'hr_percentile_10', 'hr_rmssd_50', 'total_sleep']
collection_cc = client.rnd_spark_test.cc_nightly_features_with_tcclogging_EU_US_2021_04_01_14_47_03

# columns = cc_cols
sample_size = 3000000
cut_magnitude = 0

df_ttc = query_pipeline(ttc_cols, collection_ttc, sample_size, 'ttc')
df_cc = query_pipeline(cc_cols, collection_cc, sample_size, 'cc')

df_ttc = df_ttc.rename(columns={ttc_cols[i]: cc_cols[i] for i in range(len(ttc_cols))})

df = pd.concat([df_ttc, df_cc], ignore_index=True)
df["source"] = df["source"].astype("category")

try:
    os.mkdir(f'figure/{sample_size}')

except Exception:
    pass

for col in cc_cols:

    fig, ax = plt.subplots()

    if col == "temp_skin_percentile_99":
        sns.violinplot(ax=ax, x='source', y=col,
                       data=df[(df['temp_skin_percentile_99']<50) & (df['temp_skin_percentile_99']>20)],
                       palette='Set2', cut=cut_magnitude)
    else:
        sns.violinplot(ax=ax, x='source', y=col, data=df, palette='Set2', cut=cut_magnitude)


    plt.savefig(f'figure/{sample_size}/violin_combined_{col}_sample_{sample_size}_cut_{cut_magnitude}.png')
    plt.close()
    break

print("hello")
