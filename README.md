# Description:

This minimal repository serves as a common point of version control for everyone making changes to the PySpark/Jupyter script that merges logs from the Ava Concieve (TTC), `health` collections with data from the Ava Prevent (CC), `features` collection.

# Setup:
The document describing the initial AWS EMR/notebook setup is described [here](https://avawomen.atlassian.net/wiki/spaces/DA/pages/2692808714/Flexible+data+exploration+at+scale+using+Spark+notebooks+on+EMR+for+Data+Scientists).

Once the initial setup is completed and the cluster/notebook environment is live, copy the notebook to the cluster, fill in your mongo user/pass and run the cells. 

# Debugging: 

In case of errors, or unexpected behaviour, please use the cells marked with `## DEBUG` to get a better idea of the schema structure, check for duplicate entries after merge, or test your mongo connection (requires Python kernel, won't work in a PySpark kernel)